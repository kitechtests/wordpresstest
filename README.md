Topic: Render Video Gallery from Youtube

ENDPOINT: https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&order=date&playlistId=PLdYcdVQDHNRK0eyAwBDm8EiyLrnStj0Ae&key=AIzaSyAhDYYnDwphzM-DjwpvwYg43KEX39TBKAQ

Extra Parameter: &maxResults=””

Description:

The candidate has to create a basic WordPress theme and display the results of a YouTube playlist on a page.

In order to achieve this they will need to fetch the feed from the given YouTube endpoint and display the results on the front page of the site.

The candidate will need to create a shortcode or widget to display the feed.

The test will require the candidate to use Bootstrap for the styling of the page, jQuery with AJAX to fetch the results from YouTube and WordPress to create the site.

Extra points:

* If the candidate has created a shortcode, please create a widget and vice-versa.
* Display the description and title of the video and make it play in a bootstrap modal.
* Limit the number of videos based on a parameter set in shortcode or widget.